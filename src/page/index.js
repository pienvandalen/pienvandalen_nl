import EventBus from '@/eventbus'

const page = {
  props: ['transitionName'],
  data () {
    return {
      transition: this.transitionName,
      title: this.$route.meta.title
    }
  },
  methods: {
    // The getIndex method used in beforeRouteLeave
    getIndex (routePath) {
      let routes = this.$router.options.routes
      let index
      for (let i = 0; i < routes.length; i++) {
        if (routes[i].path === routePath) {
          index = i
          break
        }
      }

      return index
    }
  },
  beforeRouteLeave (to, from, next) {
    // Decide whether to slide left or right,
    // based on the index of the from and to pages
    if (this.getIndex(from.path) < this.getIndex(to.path)) {
      this.transition = 'slide-left'
    } else {
      this.transition = 'slide-right'
    }
    // Emit an event to catch it in App and pass the arg this.transition
    EventBus.$emit('setTransition', this.transition)
    next()
  }
}

export default page
