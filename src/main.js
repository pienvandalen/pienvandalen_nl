// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Es6Promise from 'es6-promise'
import VueAutosize from 'vue-autosize'
import VeeValidate from 'vee-validate'
import * as VueGoogleMaps from 'vue2-google-maps'
import ViewLeft from '@/components/ViewLeft'
import ViewRight from '@/components/ViewRight'
import ViewCenter from '@/components/ViewCenter'
import MyMap from '@/components/MyMap'

Es6Promise.polyfill()

Vue.config.productionTip = false

Vue.use(VueAutosize)
Vue.use(VeeValidate)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBVdiI2UT8JXiur-D6wt1ape4Ex6IN1gHc',
    installComponents: true
  }
})

// Register components
Vue.component('view-left', ViewLeft)
Vue.component('view-right', ViewRight)
Vue.component('view-center', ViewCenter)
Vue.component('my-map', MyMap)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
