const mouseEventMethods = {
  data () {
    return {
    }
  },
  methods: {
    // Method to add a 'touched' class
    addTouchedClass (e) {
      this.addClass(e.currentTarget, 'touched')
      this.removeTouchedClass()
    },
    // Method te remove all 'touched' classes
    removeTouchedClass () {
      this.$nextTick(() => {
        setTimeout(() => {
          let touchedElements = document.querySelectorAll('.touched')

          setTimeout(() => {
            for (let i = 0; i < touchedElements.length; i++) {
              this.removeClass(touchedElements[i], 'touched')
              this.removeClass(touchedElements[i], 'hover')
            }
          }, 250)
        }, 0)
      })
    },
    // Method to add a 'hover' class
    mouseOverEffect (e) {
      let currentTarget = e.currentTarget
      let currentClass = currentTarget.getAttribute('class')

      if ((currentClass.indexOf('router-link-active') === -1) ||
          (currentClass.indexOf('logo') > -1)) {
        this.addClass(e.currentTarget, 'hover')
      }
    },
    // Method to remove the 'hover' class
    mouseOutEffect (e) {
      this.removeClass(e.currentTarget, 'hover')
    },
    // Method to add a class to an element
    addClass (currentElement, className) {
      let currentClassName = currentElement.getAttribute('class')

      if (typeof currentClassName !== 'undefined' && currentClassName) {
        let class2RemoveIndex = currentClassName.indexOf(className)
        if (class2RemoveIndex === -1) {
          currentElement.setAttribute('class', currentClassName + ' ' + className)
        }
      } else {
        currentElement.setAttribute('class', className)
      }
    },
    // Method to remove a class from an element
    removeClass (currentElement, className) {
      let currentClassName = currentElement.getAttribute('class')

      if (typeof currentClassName !== 'undefined' && currentClassName) {
        let class2RemoveIndex = currentClassName.indexOf(className)
        if (class2RemoveIndex !== -1) {
          let class2Remove = currentClassName.substr(class2RemoveIndex, className.length)
          let updatedClassName = currentClassName.replace(class2Remove, '').trim()
          currentElement.setAttribute('class', updatedClassName)
        }
      } else {
        currentElement.removeAttribute(className)
      }
    }
  }
}

export default mouseEventMethods
