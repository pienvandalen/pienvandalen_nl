import Vue from 'vue'
import Router from 'vue-router'
import EventBus from '@/eventbus'

// Lazy loading of components
const Home = () => import('@/components/Home')
const Gevolg = () => import('@/components/Gevolg')
const Project = () => import('@/components/Project')
const ParentProjects = () => import('@/components/ParentProjects')
const Oorzaak = () => import('@/components/Oorzaak')
const Verband = () => import('@/components/Verband')
const PageNotFound = () => import('@/components/PageNotFound')

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        title: 'Pien van Dalen',
        titleExtension: false,
        description: 'Portfoliowebsite van frontend developer en designer Pien van Dalen. Ik hou van maken. Designs, logo\'s, websites, applicaties - vanalles eigenlijk.',
        robots: 'noopd'
      }
    },
    {
      path: '/gevolg',
      component: ParentProjects,
      children: [
        {
          path: '/',
          name: 'Gevolg',
          component: Gevolg,
          meta: {
            title: 'Iemand die houdt van maken, die maakt wat',
            description: 'Zie hier het gevolg van de dingen die ik maak; mijn portfolio. Logo\'s, designs, Java-projecten, websites, tekeningen, gerechten, etc.',
            robots: 'noopd'
          }
        },
        {
          path: 'java',
          name: 'Java',
          component: Project,
          meta: {
            title: 'Java',
            description: 'Ik ben begonnen met Java leren en wat je hier ziet is een van de eerste applicaties die ik gemaakt heb: een battleship game.',
            content: 'Ik ben begonnen met Java leren en wat je hier ziet is een van de eerste applicaties die ik gemaakt heb: een battleship game. Het spel gaat uit van een raster van 7×7 waarop drie schepen staan gepositioneerd. De game werkt puur in de console en een volgende stap is iets creëren met een user interface. Voor de basis Java-kennis gebruik ik het boek Head First Java (verschrikkelijk slechte humor maar heerlijk goed te volgen).',
            image: require('../assets/img/java.jpg'),
            alt: '',
            image2: require('../assets/img/java-battleship-game.jpg'),
            alt2: 'Java Battleship game',
            robots: 'noopd'
          }
        },
        {
          path: 'web-development',
          name: 'Web development',
          component: Project,
          meta: {
            title: 'Web development',
            description: 'Deze website heb ik ontwikkeld als Progressive Web App gebouwd in Vue 2.',
            content: 'Pienvandalen.nl heb ik ontwikkeld in Vue 2, een JavaScript framework waar ik nieuwsgierig naar was. Ik heb van deze site een Progressive Web App (PWA) gemaakt. Dit maakt mogelijk dat mijn site offline blijft functioneren omdat de HTML, JS, CSS en images lokaal opgeslagen worden (als de user de site eenmaal heeft bezocht). De files worden Gzipped aangeleverd, mits de browser Gzip ondersteunt, om een betere compressie, een hogere performance en een laag dataverbruik te waarborgen. Alles voor het gebruiksgemak.',
            image: require('../assets/img/web-development.jpg'),
            alt: 'Web development',
            image2: require('../assets/img/web-development-components.png'),
            alt2: 'Web development componenten',
            robots: 'noopd'
          }
        },
        {
          path: 'logos',
          name: 'Logo\'s',
          component: Project,
          meta: {
            title: 'Logo\'s',
            description: 'Dit logo heb ik gemaakt voor een project waar ik mee bezig ben: Find a bite. Ik wilde graag een illustratie als logo maken met de uitstraling van een ets.',
            content: 'Dit logo heb ik gemaakt voor een project waar ik mee bezig ben: Find a bite. Find a bite moet een webapplicatie worden waar je snel recepten kunt vinden en toevoegen. Met behulp van Elasticsearch en AngularJS worden zoekresultaten realtime geupdatet terwijl je bijvoorbeeld ingrediënten of benodigdheden typt in het invoerveld. Wat ik gemaakt heb is vrij complex voor een logo. Dat was echter ook het plan: ik ging bewust proberen een illustratie als logo te maken met de uitstraling van een ets.',
            image: require('../assets/img/find-a-bite.jpg'),
            alt: 'Logo voor Find a bite',
            image2: require('../assets/img/find-a-bite-isometric.jpg'),
            alt2: 'Logo voor Find a bite in isometrische weergave',
            robots: 'noopd'
          }
        },
        {
          path: 'groninger-hamburger',
          name: 'Hamburger',
          component: Project,
          meta: {
            title: 'Groninger hamburger',
            description: 'De Groninger hamburger is een hamburger die ik gemaakt heb met heerlijke kruiden die vergelijkbaar zijn met de ingrediënten van de Groningse droge worst.',
            content: 'Vrij snel nadat ik in het noorden was gaan wonen, viel mij op dat er hier veel droge worst wordt genuttigd. Inmiddels ben ik fan van de Groningse droge worst en daarom heb ik deze hamburger die heerlijke kruidnagelsmaak gegeven. De samenstelling van het kruidenmengsel is vergelijkbaar met de ingrediënten van de Groningse droge worst en daarom heb ik hem omgedoopt tot de Groninger hamburger. En hey, koken is ook maken.',
            image: require('../assets/img/groninger-hamburger.jpg'),
            alt: 'De Groninger hamburger',
            image2: require('../assets/img/groninger-hamburger-kruiden.jpg'),
            alt2: 'Kruiden voor de Groninger hamburger',
            robots: 'noopd'
          }
        }
      ]
    },
    {
      path: '/oorzaak',
      name: 'Oorzaak',
      component: Oorzaak,
      meta: {
        title: 'De maaksels op de site zijn powered by Pien',
        description: 'Ik ben Pien van Dalen, de oorzaak van alles wat je op deze site ziet, aangenaam. Ik ben frontend developer, designer en student Java.',
        robots: 'noopd'
      }
    },
    {
      path: '/verband',
      name: 'Verband',
      component: Verband,
      meta: {
        title: 'Ook verband houden met de oorzaak?',
        description: 'Ook verband houden met de oorzaak? Stuur me een bericht of bekijk mijn adres- en contactgegevens hier.',
        robots: 'noopd'
      }
    },
    {
      path: '*',
      name: 'Pagina niet gevonden',
      component: PageNotFound,
      meta: {
        title: 'Pagina niet gevonden',
        description: 'De pagina kan niet gevonden worden omdat deze niet bestaat.',
        robots: 'noindex,nofollow'
      }
    }
  ]
})

// Make sure the title and meta tags get updated per page
router.beforeEach((to, from, next) => {
  let title = (to.meta.titleExtension === false) ? to.meta.title : to.meta.title + ' ◆ Pien van Dalen'

  // Chrome on iOS:
  // - removes title on pushState, so the title needs to be set after.
  // - will not set the title if it’s identical when trimmed, so
  //   appending a space won't do, but a non-breaking space works
  const ua = navigator.userAgent
  const isChromeOnIOS = ua.indexOf(' CriOS/') > -1

  if (isChromeOnIOS) {
    setTimeout(() => {
      document.title = title + String.fromCharCode(160)
    }, 100)
  } else {
    document.title = title
  }

  document.getElementsByName('description')[0].content = to.meta.description
  document.getElementsByName('robots')[0].content = to.meta.robots
  document.getElementsByName('googlebot')[0].content = to.meta.robots
  next()
})

router.afterEach((to, from) => {
  // After a route change, emit the afterRoute event, to catch these in App
  EventBus.$emit('afterRoute')
})

export default router
